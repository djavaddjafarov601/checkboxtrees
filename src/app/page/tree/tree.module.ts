import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TreeRoutingModule } from './tree-routing.module';
import { TreeComponent } from './tree.component';
import { TreeViewComponent } from './components/tree-view/tree-view.component';


@NgModule({
  declarations: [
    TreeComponent,
    TreeViewComponent
  ],
  imports: [
    CommonModule,
    TreeRoutingModule
  ]
})
export class TreeModule { }
