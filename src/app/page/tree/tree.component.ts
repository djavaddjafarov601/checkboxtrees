import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss']
})
export class TreeComponent implements OnInit {

  arr = [
  {
    id: 1, //родитель
    name: "Родитель",
    checked: true,
  },
  {
    parentId: 1,
    id: 10,
    name: "Дочерний",
    checked: true,
  },
  {
    parentId: 10,
    id: 21,
    name: "Дочерний",
    checked: true,
  },
  {
    parentId: 10,
    id: 22,
    name: "Дочерний",
    checked: true,
  },
  {
    parentId: 10,
    id: 23,
    name: "Дочерний",
    checked: true,
  },
  {
    parentId: 21,
    id: 31,
    name: "Дочерний",
    checked: true,
  },
  {
    id: 3,
    name: "Родитель",
    checked: true,
  },
  {
    parentId: 2,
    id: 20,
    name: "Дочерний",
    checked: true,
  },
  {
    parentId: 3, //!!дочерний элемент, ссылающийся на родитель 3
    id: 30,
    name: "Дочерний",
    checked: true,
  },
  {
    parentId: 3,
    id: 31,
    name: "Дочерний",
    checked: true,
  },
  ];
  arr2: any;
  constructor() { }

  ngOnInit() {
    this.arr2 = this.tree(this.arr);
  }


  // Функция преобразования изначального плоского массива во вложенные объекты
  tree(arr: any): any {
    return arr.reduce((a: any, c: any) => {
      c.children = arr.filter((i: any) => i.parentId == c.id);
      if (c.children.length !== 0) {
        a.push(c);
      } else {
        delete c.children;
      }
      return a;
    }, [])
    .filter ((i: any) => i.parentId == null)
  }

}
