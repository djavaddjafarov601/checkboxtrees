import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tree-view',
  templateUrl: './tree-view.component.html',
  styleUrls: ['./tree-view.component.scss'],
})
export class TreeViewComponent implements OnInit, OnChanges{

  item: any;
  @Input() arr: any;
  isFlag: any;
  @Output() onChanged = new EventEmitter<boolean>();

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(): void {}

  checkedValue(item: any) {
    this.item = item;
    return item.checked;
  }

  updateCheckedValue(item: any): void {
    item.checked = !item.checked;
    this.item = item;
    if (this.item.parentId) {
      this.onChanged.emit(item.checked);
    }
  }

  recursia(arr: any) {
    let checkedValue: any;
    let recurs = (arr: any) => {
        if (Array.isArray(arr)) {
            arr.forEach( elem => {
              if (!checkedValue) {
                  checkedValue = elem.checked
              } else {
                  checkedValue = elem.checked && checkedValue;
                  if (Array.isArray(elem.children)) {
                      checkedValue = checkedValue && recurs(elem.children);
                  }
              }
            })
        } else {
            if (!checkedValue) {
                checkedValue = arr.checked
            }
            if (Array.isArray(arr.children)) {
                checkedValue = checkedValue && recurs(arr.children);
            }
        }
        return checkedValue;
    }
     this.isFlag = !recurs(arr);
  }

  emitUp(item: any) {
    if (this.item.parentId) {
      // console.log("childItem", this.item);
      this.onChanged.emit(this.isFlag);
    } else {
      this.recursia(this.item);
    }
  }

}
